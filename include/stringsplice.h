// IMPORT STANDARD LIBRARIES
#include <string>
#include <vector>
#include <tuple>


using Tokens = std::vector<std::string>;


std::string splice_index(std::string const &text, std::string const &stride, char const delimiter='_');


class NonIntegerError : public std::runtime_error
{
public:
    NonIntegerError(std::string const &message="") : std::runtime_error(message) {}
};
