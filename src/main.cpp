// IMPORT STANDARD LIBRARIES
#include <iostream>

// IMPORT LOCAL LIBRARIES
#include "stringsplice.h"


int main() {
    std::string name = "test_name_here_else";
    std::string stride = "1:-1:-2";

    auto output = splice_index(name, stride);

    std::cout << "Before: \"" << name << "\"" << std::endl;
    std::cout << "After : \"" << output << "\"" << std::endl;

    return 0;
}
