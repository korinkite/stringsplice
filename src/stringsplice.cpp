// IMPORT STANDARD LIBRARIES
#include <algorithm>
#include <utility>
#include <sstream>

// IMPORT LOCAL LIBRARIES
#include "stringsplice.h"


// Combine a vector of strings into a single string
std::string join(Tokens const &items, char const &text) {
    std::string output = "";

    for (Tokens::const_iterator pointer = items.begin();
            pointer != items.end();
            ++pointer) {

        output += *pointer;

        if (pointer != items.end() - 1) {
            output += text;
        }
    }

    return output;
}


// Break apart a string into a containereat each occurrence of a character
//
// Reference: http://ysonggit.github.io/coding/2014/12/16/split-a-string-using-c.htm
//
Tokens split(std::string const &path, char delimiter) {
    std::stringstream stream {path};
    std::string item;
    Tokens tokens;

    while (getline(stream, item, delimiter)) {
        tokens.push_back(item);
    }

    return tokens;
}

// Recommend an index number, given a container of strings which represent indexes
int get_number(Tokens tokens, int index, int size, int fallback=0) {
    if (index >= size) {
        return fallback;
    }

    auto input = tokens[index];

    if (input.empty()) {
        return fallback;
    }

    try {
        return std::stoi(input);
    } catch (std::logic_error const &error) {
        throw NonIntegerError("Index: \"" + input + "\" is not an integer.");
    }
}

// Get the min:max:step value for some string
//
// Examples:
//     1: - min: 1, max: -1, step: 1
//     1:2:1 - min: 1, max: 2, step: 1
//     ::-2 - min: 0, max: -1, step: -2
//     1::3 - min: 1, max: -1, step: 3
//
std::tuple<int, int, int> gather_stride(std::string const &stride) {
    auto items = split(stride, ':');
    auto token_size = static_cast<int>(items.size());

    auto min = get_number(items, 0, token_size, 0);
    auto max = get_number(items, 1, token_size, -1);
    auto step = get_number(items, 2, token_size, 1);

    return std::make_tuple(min, max, step);
}


// Create an absolute index value
int normalize_index(int number, int size) {
    if (number < 0) {
        // We +1 here because, whenever we grab indexes, we need to include the
        // current index. -1, for example, would actually exclude the last index
        //
        number = size + number + 1;
    }

    return std::min(number, size);
}


bool less_than(int first, int second) {
    return (first < second);
}


bool greater_than_equals(int first, int second) {
    return (first >= second);
}


std::string get_text(Tokens tokens, std::string const &index) {
    int int_index;

    try {
        int_index = std::stoi(index);
    } catch (std::logic_error const &error) {
        throw NonIntegerError("Index: \"" + index + "\" is not an integer.");
    }

    return tokens[int_index];
}


bool is_stride(std::string const &stride) {
    return (stride.find(':') != std::string::npos);
}


std::string splice_index(std::string const &text, std::string const &stride, char const delimiter /* ='_' */) {
    auto tokens = split(text, delimiter);

    if (stride.empty()) {
        throw NonIntegerError("stride cannot be empty.");
    }

    if (!is_stride(stride)) {
        return get_text(tokens, stride);
    }

    auto token_size = static_cast<int>(tokens.size());
    auto info = gather_stride(stride);

    auto min = normalize_index(std::get<0>(info), token_size);
    auto max = normalize_index(std::get<1>(info), token_size);

    auto step = std::get<2>(info);

    // TODO: I was getting a compiler warning using lambdas so I decided to
    //       just use an actual function ...
    //
    auto comparison = less_than;

    if (step < 0) {
        std::swap(min, max);
        comparison = greater_than_equals;

        // Note: We need to minus 1 here because, for example, if we did 1:-1:-1
        //       then the minimum would be -1, which is always the size of the
        //       number of tokens. So it'd always index error. We -1 here to
        //       make sure that doesn't happen.
        //
        min -= 1;
    }

    // TODO: Calculate the number of items needed and add sections.reserve(N)
    Tokens sections;
    for(int index = min; comparison(index, max); index += step) {
        sections.push_back(tokens[index]);
    }

    return join(sections, delimiter);
}
