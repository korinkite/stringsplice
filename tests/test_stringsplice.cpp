// IMPORT THIRD-PARTY LIBRARIES
#include "gtest/gtest.h"

// IMPORT LOCAL LIBRARIES
#include "stringsplice.h"


// Get a single index
TEST(single_index, input) {
    EXPECT_EQ("name", splice_index("test_name_here", "1"));
}


// Split a string so that the result equals itself
TEST(equal_string, input) {
    EXPECT_EQ("test_name_here", splice_index("test_name_here", "0:3:1"));
}


// Split an empty string so that the result equals itself
TEST(equal_empty_string, input) {
    EXPECT_EQ("", splice_index("", "0:0:1"));
}


// Split a string that only defines a start index of 0
TEST(start_index_001, input) {
    EXPECT_EQ("test_name_here", splice_index("test_name_here", "0:"));
}

// Split a string that defines a start index of 1
TEST(start_index_002, input) {
    EXPECT_EQ("name_here", splice_index("test_name_here", "1:"));
}


// Split a string that only defines a start index of -1
TEST(start_index_003, input) {
    EXPECT_EQ("", splice_index("test_name_here", "-1:"));
}


// Split a string that only defines a start index of -2
TEST(start_index_004, input) {
    EXPECT_EQ("here", splice_index("test_name_here", "-2:"));
}


// Split a string and capture every 2nd token
TEST(stride_1, input) {
    EXPECT_EQ("test_here", splice_index("test_name_here", "0::2"));
}


// Split a string and reverse the order of the tokens
TEST(reverse, input) {
    EXPECT_EQ("here_name_test", splice_index("test_name_here", "::-1"));
}


TEST(complex_001, input) {
    EXPECT_EQ("here", splice_index("test_name_here", "-2::"));
}


TEST(complex_002, input) {
    EXPECT_EQ("here", splice_index("test_name_here", "-2::"));
}


TEST(complex_003, input) {
    EXPECT_EQ("else_name", splice_index("test_name_here_else", "1:-1:-2"));
}


// Split a string using negative input
TEST(negative, input) {
    EXPECT_EQ("test_name_here", splice_index("test_name_here", "0:-1:1"));
}


TEST(over_range, input) {
    EXPECT_EQ("test_name_here", splice_index("test_name_here", "0:3:1"));
}


// Split an empty string what a range that is greater than its length of tokens
TEST(over_range_empty, input) {
    EXPECT_EQ("", splice_index("", "0:3:1"));
}


// Fail to get a single index because the user did not provide a single index
TEST(bad_input_single_index, input) {
    EXPECT_THROW(splice_index("test_name_here", "a"), NonIntegerError);
}


// Fail to get a single index because the user did not provide a single index
TEST(bad_input_no_stride, input) {
    EXPECT_THROW(splice_index("test_name_here", ""), NonIntegerError);
}


// Fail to get a single index because the user did not provide a single index
TEST(bad_input_bad_index, input) {
    EXPECT_THROW(splice_index("test_name_here", "a:1:1"), NonIntegerError);
}


// Fail to get a single index because the user did not provide a single index
TEST(bad_range_binary, input) {
    EXPECT_EQ("", splice_index("test_name_here", "-1:1"));
}


// Fail to get a single index because the user did not provide a single index
TEST(bad_range_ternary, input) {
    EXPECT_EQ("", splice_index("test_name_here", "-1:1:-1"));
}
